FROM frolvlad/alpine-oraclejdk8:slim
MAINTAINER Manuel Zegarra "ikopecel@ebizlatin.com"
ENV REFRESHED_AT 2017-07-01 23:20
#VOLUME /tmp
#VOLUME /Apps/arca/ms-customer
ADD ./com.ebiz.msrfqproveedordetalles/com.ebiz.msrfqproveedordetalles.rest/target/com.ebiz.msrfqproveedordetalles.rest-1.0-SNAPSHOT.jar app.jar
RUN sh -c 'touch /app.jar'
EXPOSE 8080
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java -Xmx32m -Xss256k $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]
