package com.ebiz.msrfqproveedordetalles;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class Rfqproveedordetalles {
	
	@JsonProperty("id")
	private int id;
	
	@JsonProperty("description")
	private String description;
	/////////////////////////7
	///XC CABECERA
	@JsonProperty("nroreq")
	private int nroreq;

	@JsonProperty("prioridad")
	private String prioridad;

	@JsonProperty("observacion")
	private String observacion;

	@JsonProperty("almacen")
	private String almacen;

	@JsonProperty("propietario")
	private String propietario;

	@JsonProperty("fechainicio")
	private String fechainicio;

	@JsonProperty("fechafin")
	private String fechafin;

	@JsonProperty("moneda")
	private String moneda;

	@JsonProperty("estado")
	private String estado;
	///XC condiciones generales
	@JsonProperty("descuento")
	private String descuento;

	@JsonProperty("impuesto")
	private String impuesto;

	@JsonProperty("otroscostos")
	private String otroscostos;

	@JsonProperty("direcciondeentrega")
	private String direcciondeentrega;

	@JsonProperty("terminosdepago")
	private String terminosdepago;

	@JsonProperty("tipodeprecio")
	private String tipodeprecio;
	///XC Detalle
	@JsonProperty("productos")
	private List<Productoxrfq> productos;
	///XC Otras consideraciones
	@JsonProperty("area")
	private String area;
	
	@JsonProperty("mensaje")
	private String mensaje;

	@JsonProperty("notas")
	private String notas;

	@JsonProperty("comentarios")
	private String comentarios;

	@JsonProperty("docadjuntos")
	private String docadjuntos;	
	////////////////////////////
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	/////////cabecera
	public int getNroreq() {
		return nroreq;
	}

	public void setNroreq(int nroreq) {
		this.nroreq = nroreq;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getAlmacen() {
		return almacen;
	}

	public void setAlmacen(String almacen) {
		this.almacen = almacen;
	}

	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

	public String getFechainicio() {
		return fechainicio;
	}

	public void setFechainicio(String fechainicio) {
		this.fechainicio = fechainicio;
	}

	public String getFechafin() {
		return fechafin;
	}

	public void setFechafin(String fechafin) {
		this.fechafin = fechafin;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	///condiciones generales
	public String getDescuento() {
		return descuento;
	}

	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}

	public String getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(String impuesto) {
		this.impuesto = impuesto;
	}

	public String getOtroscostos() {
		return otroscostos;
	}

	public void setOtroscostos(String otroscostos) {
		this.otroscostos = otroscostos;
	}

	public String getDirecciondeentrega() {
		return direcciondeentrega;
	}

	public void setDirecciondeentrega(String direcciondeentrega) {
		this.direcciondeentrega = direcciondeentrega;
	}

	public String getTerminosdepago() {
		return terminosdepago;
	}

	public void setTerminosdepago(String terminosdepago) {
		this.terminosdepago = terminosdepago;
	}

	public String getTipodeprecio() {
		return tipodeprecio;
	}

	public void setTipodeprecio(String tipodeprecio) {
		this.tipodeprecio = tipodeprecio;
	}
	///detalle
	public List<Productoxrfq> getProductos() {
		return productos;
	}

	public void setProductos(List<Productoxrfq> productos) {
		this.productos = productos;
	}
	///otras consideraciones
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public String getDocadjuntos() {
		return docadjuntos;
	}

	public void setDocadjuntos(String docadjuntos) {
		this.docadjuntos = docadjuntos;
	}
	////////////////
	
	
}
