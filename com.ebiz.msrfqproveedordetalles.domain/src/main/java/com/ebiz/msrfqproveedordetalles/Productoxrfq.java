package com.ebiz.msrfqproveedordetalles;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Productoxrfq {
	
	@JsonProperty("id")
	private int id;
	
	@JsonProperty("description")
	private String description;
	/////////////////////////7
	///XC CABECERA
	@JsonProperty("codigoproducto")
	private int codigoproducto;

	@JsonProperty("nombreproducto")
	private String nombreproducto;

	@JsonProperty("descripcionproducto")
	private String descripcionproducto;

	@JsonProperty("nroparte")
	private String nroparte;

	@JsonProperty("cantidad")
	private String cantidad;

	@JsonProperty("unidad")
	private String unidad;
	////////////////////////////
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	/////////cabecera
	public int getCodigoproducto() {
		return codigoproducto;
	}


	public void setCodigoproducto(int codigoproducto) {
		this.codigoproducto = codigoproducto;
	}


	public String getNombreproducto() {
		return nombreproducto;
	}


	public void setNombreproducto(String nombreproducto) {
		this.nombreproducto = nombreproducto;
	}


	public String getDescripcionproducto() {
		return descripcionproducto;
	}


	public void setDescripcionproducto(String descripcionproducto) {
		this.descripcionproducto = descripcionproducto;
	}


	public String getNroparte() {
		return nroparte;
	}

	public void setNroparte(String nroparte) {
		this.nroparte = nroparte;
	}


	public String getCantidad() {
		return cantidad;
	}


	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}


	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}
	////////////////
	
	
}
