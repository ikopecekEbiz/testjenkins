package com.ebiz.msrfqproveedordetalles;

import java.util.List;

import com.ebiz.msrfqproveedordetalles.Rfqproveedordetalles;

public interface RfqproveedordetallesRepository {
	
	List<Rfqproveedordetalles> list(int id);

}