package com.ebiz.msrfqproveedordetalles.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ebiz.msrfqproveedordetalles.Rfqproveedordetalles;
import com.ebiz.msrfqproveedordetalles.RfqproveedordetallesRepository;

import com.ebiz.msrfqproveedordetalles.Productoxrfq;

@Repository
public class RfqproveedordetallesRepositoryImpl implements RfqproveedordetallesRepository {
	

	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public List<Rfqproveedordetalles> list(int id) {
		String SQL = "select IN_IDRFQ, ch_prioridad, vc_observacion, vc_almacen, vc_propietario, ts_fechainicio, ts_fechafin, in_idmoneda, ch_estado, vc_area,vc_mensaje,vc_nota,vc_comentario from rfq.t_rfq where IN_IDRFQ =?";
		//falta poner atributosxrfq
		//falta poner archivos		
		String SQL2 = "select in_idproducto,vc_descortapro, vc_deslargapro,vc_numeroparte, fl_cantidad, in_idunidad from rfq.t_productoxrfq";

		List<Rfqproveedordetalles> rfqproveedordetalless = null;
		
		List<Productoxrfq> productoxrfqs = null;

		try{
			rfqproveedordetalless = jdbcTemplate.query(SQL,new Object[] { id },this::mapParam);
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
		}

		try{
			productoxrfqs = jdbcTemplate.query(SQL2,this::mapParam2);
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
		}


		rfqproveedordetalless.get(0).setProductos(productoxrfqs);

		return rfqproveedordetalless;
		
	}
	
	public Rfqproveedordetalles mapParam(ResultSet rs, int i) throws SQLException {
		Rfqproveedordetalles rfqproveedordetalles = new Rfqproveedordetalles();
		rfqproveedordetalles.setNroreq(rs.getInt("IN_IDRFQ"));
		rfqproveedordetalles.setPrioridad(rs.getString("ch_prioridad"));
		rfqproveedordetalles.setObservacion(StringUtils.trimToEmpty(rs.getString("vc_observacion")));
		rfqproveedordetalles.setAlmacen(StringUtils.trimToEmpty(rs.getString("vc_almacen")));
		rfqproveedordetalles.setPropietario(StringUtils.trimToEmpty(rs.getString("vc_propietario")));
		rfqproveedordetalles.setFechainicio(StringUtils.trimToEmpty(rs.getString("ts_fechainicio")));
		rfqproveedordetalles.setFechafin(StringUtils.trimToEmpty(rs.getString("ts_fechafin")));
		rfqproveedordetalles.setMoneda(StringUtils.trimToEmpty(rs.getString("in_idmoneda")));
		rfqproveedordetalles.setEstado(StringUtils.trimToEmpty(rs.getString("ch_estado")));
		rfqproveedordetalles.setArea(StringUtils.trimToEmpty(rs.getString("vc_area")));
		rfqproveedordetalles.setMensaje(StringUtils.trimToEmpty(rs.getString("vc_mensaje")));
		rfqproveedordetalles.setNotas(StringUtils.trimToEmpty(rs.getString("vc_nota")));
		rfqproveedordetalles.setComentarios(StringUtils.trimToEmpty(rs.getString("vc_comentario")));

		return rfqproveedordetalles;
	}

	public Productoxrfq mapParam2(ResultSet rs, int i) throws SQLException {
		Productoxrfq productoxrfq = new Productoxrfq();
		productoxrfq.setCodigoproducto(rs.getInt("in_idproducto"));
		productoxrfq.setNombreproducto(StringUtils.trimToEmpty(rs.getString("vc_descortapro")));
		productoxrfq.setDescripcionproducto(StringUtils.trimToEmpty(rs.getString("vc_deslargapro")));
		productoxrfq.setNroparte(StringUtils.trimToEmpty(rs.getString("vc_numeroparte")));
		productoxrfq.setCantidad(StringUtils.trimToEmpty(rs.getString("fl_cantidad")));
		productoxrfq.setUnidad(StringUtils.trimToEmpty(rs.getString("in_idunidad")));

		return productoxrfq;
	}
}
