package com.ebiz.msrfqproveedordetalles.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ebiz.msrfqproveedordetalles.Rfqproveedordetalles;
import com.ebiz.msrfqproveedordetalles.RfqproveedordetallesService;

@RestController()
@RequestMapping("/rfqprov")
public class RfqproveedordetallesController {
	
	@Autowired
	private RfqproveedordetallesService rfqproveedordetallesService;
	
	
	@RequestMapping(value = "/detalle/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<Rfqproveedordetalles> listAll(@PathVariable("id") int id) {
		
		
		return rfqproveedordetallesService.list(id);
		
	}
	

}
