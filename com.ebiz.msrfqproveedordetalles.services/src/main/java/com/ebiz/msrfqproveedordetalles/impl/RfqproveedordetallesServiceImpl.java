package com.ebiz.msrfqproveedordetalles.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ebiz.msrfqproveedordetalles.Rfqproveedordetalles;
import com.ebiz.msrfqproveedordetalles.RfqproveedordetallesRepository;
import com.ebiz.msrfqproveedordetalles.RfqproveedordetallesService;

@Service
public class RfqproveedordetallesServiceImpl implements RfqproveedordetallesService {
	
	@Autowired
	private RfqproveedordetallesRepository rfqproveedordetallesRepository;
	
	public List<Rfqproveedordetalles> list(int id) {
		
		return rfqproveedordetallesRepository.list(id);
	}

}
