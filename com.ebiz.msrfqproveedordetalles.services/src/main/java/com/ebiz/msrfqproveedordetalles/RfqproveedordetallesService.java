package com.ebiz.msrfqproveedordetalles;


import java.util.List;

import com.ebiz.msrfqproveedordetalles.Rfqproveedordetalles;

public interface RfqproveedordetallesService {
	
	List<Rfqproveedordetalles> list(int id);

}
